package cjg.nn.hopfield.main;

import javax.swing.JApplet;

import cjg.nn.hopfield.ui.AppPanel;

@SuppressWarnings("serial")
public class AppletMain extends JApplet {
	@Override
	public String getAppletInfo() {
		return "This is a small Hopfield-Net demo.\nYou can download the jar and execute the demo as a runnable jar file.";
	}

	@Override
	public String[][] getParameterInfo() {
		return new String[][] {
				{ "patRenderSize", "[8;300]",
				"The default pattern render size to use." },
				{ "[", "[2;100]", "The default pattern width." },
				{ "patHeight", "[1;100]", "The default patern height." },
				{ "defPatCount", "[0;30]",
				"The number of default genderated start patterns." },
				{ "disAm", "[1;100]",
				"The amount of disturbance applied to the patterns." } };
	}

	@Override
	public void init() {
		for (final String[] inf : getParameterInfo())
			if (getParameter(inf[0]) != null) {
				System.setProperty(inf[0], getParameter(inf[0]));
			}
		add(new AppPanel());
	}
}
