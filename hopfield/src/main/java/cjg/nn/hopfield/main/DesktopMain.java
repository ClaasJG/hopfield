package cjg.nn.hopfield.main;

import javax.swing.JFrame;

import cjg.nn.hopfield.ui.AppPanel;

public class DesktopMain {
	public static void main(final String[] args) {
		final JFrame frame = new JFrame("Hopfield-Net");
		frame.setContentPane(new AppPanel());
		frame.setSize(600, 300);
		frame.setDefaultCloseOperation(2);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
	}
}
