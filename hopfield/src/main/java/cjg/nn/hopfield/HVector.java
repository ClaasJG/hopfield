package cjg.nn.hopfield;

import java.util.Arrays;

public abstract class HVector {

	private static class DefHVImp extends HVector {

		private final boolean[] vec;

		public DefHVImp(final int size) {
			super(size);
			vec = new boolean[size];
		}

		@Override
		public boolean equals(final Object o) {
			if (o == null)
				return false;
			if (o instanceof DefHVImp)
				return Arrays.equals(vec, ((DefHVImp) o).vec);
			else
				return super.equals(o);
		}

		@Override
		public boolean getValue(final int i) {
			return vec[i];
		}

		@Override
		public void setValue(final int i, final boolean value) {
			vec[i] = value;
		}

	}

	public static HVector create(final boolean... val) {
		final HVector vec = create(val.length);
		for (int i = 0, m = val.length; i < m; ++i) {
			vec.setValue(i, val[i]);
		}
		return vec;
	}

	public static HVector create(final int size) {
		return new DefHVImp(size);
	}

	private final int size;

	protected HVector(final int size) {
		this.size = size;
	}

	@Override
	public boolean equals(final Object o) {
		if (o instanceof HVector && o != null) {
			final HVector vec = (HVector) o;
			if (vec.size != size)
				return false;
			for (int i = 0; i < size; ++i)
				if (vec.getValue(i) != getValue(i))
					return false;
			return true;
		}
		return false;
	}

	public int getSize() {
		return size;
	}

	public abstract boolean getValue(int i);

	public abstract void setValue(int i, boolean value);

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < size; ++i) {
			sb.append(i == size - 1 ? getValue(i) ? 1 : -0 : (getValue(i) ? 1
					: -0) + ", ");
		}
		sb.append("]");
		return sb.toString();
	}

}
