package cjg.nn.hopfield;

import java.util.Arrays;

public abstract class HopfieldNet {

	private static class DefNetImp extends HopfieldNet {

		private final double[] mat;

		public DefNetImp(final int size) {
			super(size);
			mat = new double[size * size];
		}

		@Override
		public double getWeight(final int i, final int j) {
			return mat[getSize() * i + j];
		}

		@Override
		public void setWeight(final int i, final int j, final double value) {
			if (i != j) {
				mat[getSize() * i + j] = mat[getSize() * j + i] = value;
			}
		}

	}

	public interface RecStepCallback {

		public void step(HVector vec);

	}

	public static HopfieldNet create(final int size) {
		return new DefNetImp(size);
	}

	public static HopfieldNet createNet(final HVector... patterns) {
		int size = 0;
		for (final HVector hv : patterns) {
			size = Math.max(size, hv.getSize());
		}
		final HopfieldNet net = HopfieldNet.create(size);
		for (int n = 0, m = patterns.length; n < m; ++n) {
			final HVector vec = patterns[n];
			for (int i = 0, im = Math.min(size, vec.getSize()); i < im; ++i) {
				for (int j = 0; j < i; ++j) {
					net.setWeight(i, j, net.getWeight(i, j)
							+ (vec.getValue(i) ? 1 : -1)
							* (vec.getValue(j) ? 1 : -1));
				}
			}
		}
		return net;
	}

	private final int size;

	protected HopfieldNet(final int size) {
		this.size = size;
	}

	// Def imp...
	/** The callback may or may not be called... */
	public HVector findStableFiringPattern(final HVector hvec,
			final RecStepCallback cb) {
		boolean[] oldest = null;
		boolean[] oldVec = null;
		boolean[] actVec = new boolean[size];
		for (int i = 0, m = Math.min(hvec.getSize(), size); i < m; ++i) {
			actVec[i] = hvec.getValue(i);
		}

		HVector last;
		do {
			oldest = oldVec;
			oldVec = actVec;

			actVec = new boolean[size];
			for (int i = 0; i < size; ++i) {
				double val = 0;
				for (int j = 0; j < size; ++j) {
					val += getWeight(i, j) * (oldVec[j] ? 1 : -1);
				}
				actVec[i] = val > 0 ? true : val < 0 ? false : oldVec[i];
			}

			last = HVector.create(size);
			for (int i = 0; i < size; ++i) {
				last.setValue(i, actVec[i]);
			}
			cb.step(last);
		} while (!Arrays.equals(oldVec, actVec)
				&& !Arrays.equals(oldest, actVec));

		return last;
	}

	public int getSize() {
		return size;
	}

	public abstract double getWeight(int i, int j);

	public abstract void setWeight(int i, int j, double value);

	@Override
	public String toString() {
		int bgst1 = 0, bgst2 = 0;
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < i; ++j) {
				final String val = String.valueOf(getWeight(i, j));
				final String[] prts = val.split("\\.");
				bgst1 = Math.max(bgst1, prts[0].length());
				if (prts.length > 1) {
					bgst2 = Math.max(bgst2,
							"0".equals(prts[1]) ? 0 : prts[1].length());
				}
			}
		}
		final String fF = "%" + bgst1 + "." + bgst2 + "f";
		final String sF = "%" + (bgst1 + (bgst2 == 0 ? 0 : bgst2 + 1)) + "s|";

		final StringBuilder sb = new StringBuilder();
		sb.append("Size: ").append(size);
		for (int j = 0; j < size; ++j) {
			sb.append("\n|");
			for (int i = 0; i < size; ++i) {
				sb.append(String.format(sF, String.format(fF, getWeight(i, j))));
			}
		}
		return sb.toString();
	}

}
