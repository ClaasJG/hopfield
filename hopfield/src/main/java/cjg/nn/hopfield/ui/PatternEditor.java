package cjg.nn.hopfield.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
public class PatternEditor extends JPanel {
	private static class GlassPanel extends JPanel {
		private static final Color BACK = new Color(0.8F, 0.8F, 0.8F, 0.8F);

		private GlassPanel() {
			final MouseAdapter ma = new MouseAdapter() {
			};
			addMouseMotionListener(ma);
			addMouseWheelListener(ma);
			addMouseListener(ma);
			addKeyListener(new KeyAdapter() {
			});
		}

		@Override
		protected void paintComponent(final Graphics g) {
			g.setColor(BACK);
			g.fillRect(0, 0, getWidth(), getHeight());
		}
	}

	public static void show(final JRootPane panel, final Pattern pattern,
			final Runnable finishedCB) {
		final PatternEditor pe = new PatternEditor(pattern, true);

		final Component oldPane = panel.getGlassPane();
		final JPanel glassPane = new GlassPanel();
		panel.setGlassPane(glassPane);
		glassPane.setOpaque(false);
		glassPane.setVisible(true);

		glassPane.setLayout(new BorderLayout());
		glassPane.add(pe, "Center");
		glassPane.add(Box.createVerticalStrut(20), "North");
		glassPane.add(Box.createVerticalStrut(20), "South");

		final JPanel editMode = new JPanel();
		editMode.setOpaque(false);
		editMode.setLayout(new BoxLayout(editMode, 1));
		final JToggleButton set = new JToggleButton("Set");
		set.setSelected(pe.set);
		set.addChangeListener(new ChangeListener() {
			public void stateChanged(final ChangeEvent e) {
				pe.set = set.isSelected();
			}
		});
		editMode.add(set);
		final JToggleButton singleClickChange = new JToggleButton("Click-Mode");
		singleClickChange.addChangeListener(new ChangeListener() {
			public void stateChanged(final ChangeEvent e) {
				pe.singleSet = singleClickChange.isSelected();
			}
		});
		singleClickChange.setSelected(pe.singleSet);
		final JToggleButton holdChange = new JToggleButton("Hold-Mode");
		holdChange.setSelected(!pe.singleSet);
		final ButtonGroup bg = new ButtonGroup();
		bg.add(singleClickChange);
		bg.add(holdChange);
		editMode.add(singleClickChange);
		editMode.add(holdChange);
		glassPane.add(editMode, "West");

		final JPanel finish = new JPanel();
		finish.setOpaque(false);
		final JButton back = new JButton("Back");
		glassPane.add(finish, "East");
		finish.setLayout(new BorderLayout());
		finish.add(back, "South");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				panel.setGlassPane(oldPane);
				oldPane.setVisible(false);
				if (finishedCB != null) {
					finishedCB.run();
				}
			}
		});
	}

	private Pattern pattern;

	private boolean set = true;

	private boolean singleSet = false;

	public PatternEditor(final Pattern pattern, final boolean editable) {
		this.pattern = pattern;
		if (editable) {
			addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(final MouseEvent e) {
					update(tileUnderPoint(e.getPoint()));
				}
			});
			addMouseMotionListener(new MouseAdapter() {
				@Override
				public void mouseDragged(final MouseEvent e) {
					if (!singleSet) {
						update(tileUnderPoint(e.getPoint()));
					}
				}
			});
		}
	}

	public Pattern getPattern() {
		return pattern;
	}

	private Rectangle getPatternPos() {
		final int scale = Math.min(getWidth() / pattern.getWidth(), getHeight()
				/ pattern.getHeight());
		final int width = scale * pattern.getWidth();
		final int height = scale * pattern.getHeight();
		return new Rectangle((getWidth() - width) / 2,
				(getHeight() - height) / 2, width, height);
	}

	@Override
	protected void paintComponent(final Graphics g) {
		final Rectangle pos = getPatternPos();

		PatternIcon
		.drawPattern(pattern, g, pos.x, pos.y, pos.width, pos.height);
	}

	public void setPattern(final Pattern pattern) {
		this.pattern = pattern;
		repaint();
	}

	private Point tileUnderPoint(final Point point) {
		final Rectangle rec = getPatternPos();
		if (!rec.contains(point))
			return null;
		final Point p = new Point(point);
		p.x -= rec.x;
		p.y -= rec.y;
		p.x = (int) (p.x / rec.getWidth() * pattern.getWidth());
		p.y = (int) (p.y / rec.getHeight() * pattern.getHeight());
		return p;
	}

	private void update(final Point tile) {
		if (tile != null && pattern.getValue(tile.x, tile.y) != set) {
			pattern.setValue(tile.x, tile.y, set);
			SwingUtilities.getRootPane(this).repaint();
		}
	}
}
