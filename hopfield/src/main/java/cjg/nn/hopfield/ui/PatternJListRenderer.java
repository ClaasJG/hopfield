package cjg.nn.hopfield.ui;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.WeakHashMap;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;

public class PatternJListRenderer implements ListCellRenderer<Pattern> {
	private static ListCellRenderer<Object> LAF_DEP_RENDERER = new JList<Object>()
			.getCellRenderer();

	static {
		UIManager.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(final PropertyChangeEvent evt) {
				if ("lookAndFeel".equalsIgnoreCase(evt.getPropertyName())) {
					PatternJListRenderer.LAF_DEP_RENDERER = new JList<Object>()
							.getCellRenderer();
				}
			}
		});
	}

	private final Map<Pattern, Icon> iconCatch = new WeakHashMap<Pattern, Icon>();
	private int iconSize = 128;
	private boolean showLerned = true;

	private Icon getIcon(final Pattern patt) {
		Icon ico = iconCatch.get(patt);
		if (ico == null) {
			iconCatch.put(patt, ico = new PatternIcon(patt, iconSize, iconSize,
					showLerned));
		}
		return ico;
	}

	public int getIconSize() {
		return iconSize;
	}

	public Component getListCellRendererComponent(
			final JList<? extends Pattern> list, final Pattern value,
			final int index, final boolean isSelected,
			final boolean cellHasFocus) {
		final Component comp = LAF_DEP_RENDERER.getListCellRendererComponent(
				list, value, index, isSelected, cellHasFocus);
		if (comp instanceof JLabel) {
			final JLabel lab = (JLabel) comp;
			lab.setHorizontalAlignment(0);
			lab.setText(null);
			lab.setIcon(getIcon(value));
		}
		return comp;
	}

	public boolean isShowLerned() {
		return showLerned;
	}

	public void setIconSize(final int size) {
		if (iconSize != size) {
			iconCatch.clear();
			iconSize = size;
		}
	}

	public void setShowLerned(final boolean showLerned) {
		this.showLerned = showLerned;
	}
}
