package cjg.nn.hopfield.ui;

import java.util.Arrays;

public class Pattern {
	public static Pattern create(final int width, final int height) {
		return new Pattern(width, height);
	}

	private boolean[] content;
	private final int height;

	private boolean lerned = true;

	private final int width;

	private Pattern(final int width, final int height) {
		this.width = width;
		this.height = height;
		content = new boolean[width * height];
	}

	@Override
	public Pattern clone() {
		final Pattern pat = create(width, height);
		pat.content = Arrays.copyOf(content, content.length);
		return pat;
	}

	public int getHeight() {
		return height;
	}

	public boolean getValue(final int x, final int y) {
		return content[y * width + x];
	}

	public int getWidth() {
		return width;
	}

	public boolean isLearned() {
		return lerned;
	}

	public void setLerned(final boolean lerned) {
		this.lerned = lerned;
	}

	public void setValue(final int x, final int y, final boolean value) {
		content[y * width + x] = value;
	}
}
