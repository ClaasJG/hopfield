package cjg.nn.hopfield.ui;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RenderingHints;

import javax.swing.Icon;

public class PatternIcon implements Icon {
	private static final Paint BOTTOM_HIGHLIGHT = new GradientPaint(0.0F, 0.0F,
			new Color(1.0F, 1.0F, 1.0F, 0.0F), 0.0F, 10.0F, new Color(1.0F,
					1.0F, 1.0F, 0.4F));
	private static final Color LERNED = Color.GREEN;

	private static final Color NOTLERNED = Color.RED;

	private static final Color SET = Color.DARK_GRAY;

	private static final Paint TOP_SHADOW = new GradientPaint(0.0F, 0.0F,
			new Color(0.0F, 0.0F, 0.0F, 0.4F), 0.0F, 10.0F, new Color(0.0F,
					0.0F, 0.0F, 0.0F));
	private static final Color UNSET = Color.LIGHT_GRAY;

	public static void drawPattern(final Pattern patt, final Graphics g,
			final int x, final int y, final int width, final int height) {
		final int xS = width / patt.getWidth();
		final int yS = height / patt.getHeight();
		g.translate(x, y);
		for (int xi = 0, xim = patt.getWidth(); xi < xim; xi++) {
			for (int yi = 0, yim = patt.getHeight(); yi < yim; yi++) {
				g.setColor(patt.getValue(xi, yi) ? SET : UNSET);
				g.fill3DRect(xi * xS, yi * yS, xS, yS, true);
			}
		}
		g.translate(-x, -y);
	}

	private final int height;
	private final Pattern pattern;
	private final boolean showLerned;
	private final int width;

	public PatternIcon(final Pattern patt, final int width, final int height,
			final boolean showLerned) {
		this.width = width;
		this.height = height;
		pattern = patt;
		this.showLerned = showLerned;
	}

	public int getIconHeight() {
		return height;
	}

	public int getIconWidth() {
		return width;
	}

	public void paintIcon(final java.awt.Component c, final Graphics g,
			final int x, final int y) {
		Graphics2D g2;
		drawPattern(pattern, g, x, y, width, height);
		if (!showLerned || width / 5 < 10 || height / 5 < 10)
			return;
		g2 = (Graphics2D) g.create();
		g2.translate(5 + x, 5 + y);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(Color.black);
		g2.fillOval(-1, -1, 12, 12);
		g2.setColor(pattern.isLearned() ? LERNED : NOTLERNED);
		g2.fillOval(0, 0, 10, 10);
		g2.setPaint(TOP_SHADOW);
		g2.fillOval(0, 0, 10, 10);
		g2.setPaint(BOTTOM_HIGHLIGHT);
		g2.fillOval(0, 0, 10, 10);
		g2.dispose();
		return;
	}
}
