package cjg.nn.hopfield.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import cjg.nn.hopfield.HVector;
import cjg.nn.hopfield.HopfieldNet;

@SuppressWarnings("serial")
public class AppPanel extends JPanel {
	public static int getParameter(final String name, final int lowerBound,
			final int upperBound) throws Exception {
		if (System.getProperty(name) == null)
			throw new IllegalArgumentException("Parameter '" + name
					+ "' not set!");
		final int val = Integer.valueOf(System.getProperty(name)).intValue();
		if (val < lowerBound || val > upperBound)
			throw new IllegalArgumentException(val + " is not in ["
					+ lowerBound + ";" + upperBound + "]");
		return val;
	}

	private double disturbFactor;
	private DefaultListModel<Pattern> listModel;
	private HopfieldNet net;
	private int patternHeight;
	private int patternWidth;

	private PatternEditor pe;

	public AppPanel() {
		patternWidth = 8;
		patternHeight = 8;
		disturbFactor = 0.05D;
		try {
			for (final LookAndFeelInfo fafi : UIManager
					.getInstalledLookAndFeels())
				if ("Nimbus".equalsIgnoreCase(fafi.getName())) {
					UIManager.setLookAndFeel(fafi.getClassName());
					break;
				}
		} catch (final Exception localException1) {
		}
		setLayout(new BorderLayout());

		final JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(1.0D);
		add(splitPane, "Center");

		final JPanel rightPanel = new JPanel();
		splitPane.setRightComponent(rightPanel);
		rightPanel.setLayout(new BorderLayout(0, 0));

		final JScrollPane scrollPane = new JScrollPane();
		rightPanel.add(scrollPane, "Center");

		final JList<Pattern> list = new JList<Pattern>();
		list.setVisibleRowCount(0);
		list.setModel(listModel = new DefaultListModel<Pattern>());
		final PatternJListRenderer renderer = new PatternJListRenderer();
		list.setCellRenderer(renderer);
		try {
			renderer.setIconSize(getParameter("patRenderSize", 8, 300));
		} catch (final Exception e) {
			System.err.println("Error reading 'patRenderSize'!\n\t-> "
					+ e.getMessage());
		}
		list.setToolTipText("Right-Click for context menu");
		list.setSelectionMode(0);
		addListContext(list);
		scrollPane.setViewportView(list);

		splitPane.setLeftComponent(getLeftPanel());

		final JSlider slider = new JSlider();
		slider.setMinimum(8);
		slider.setMaximum(300);
		slider.setValue(renderer.getIconSize());
		rightPanel.add(slider, "South");
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(final ChangeEvent e) {
				renderer.setIconSize(slider.getValue());
				list.updateUI();
			}
		});
		try {
			patternWidth = getParameter("patWidth", 2, 100);
		} catch (final Exception e) {
			System.err.println("Error reading 'patWidth'!\n\t-> "
					+ e.getMessage());
		}
		try {
			patternHeight = getParameter("patHeight", 1, 100);
		} catch (final Exception e) {
			System.err.println("Error reading 'patHeight'!\n\t-> "
					+ e.getMessage());
		}
		int start = 3;
		try {
			start = getParameter("defPatCount", 0, 30);
		} catch (final Exception e) {
			System.err.println("Error reading 'defPatCount'!\n\t-> "
					+ e.getMessage());
		}
		for (int i = 0; i < start; i++) {
			listModel.addElement(getPattern((char) (65 + i)));
		}
		try {
			disturbFactor = getParameter("disAm", 1, 100) / 100.0D;
		} catch (final Exception e) {
			System.err
			.println("Error reading 'disAm'!\n\t-> " + e.getMessage());
		}
		updateNet();
	}

	private void addListContext(final JList<Pattern> list) {
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					popup(e);
				}

			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					popup(e);
				}

			}

			private void popup(final MouseEvent e) {
				final int index = list.locationToIndex(e.getPoint());
				final Runnable editorFinished = new Runnable() {
					public void run() {
						updateNet();
					}
				};
				final JPopupMenu pmen = new JPopupMenu();
				if (index != -1) {
					if (list.getCellBounds(index, index).contains(e.getPoint())) {
						list.setSelectedIndex(index);
						final Pattern patt = list.getSelectedValue();
						final JCheckBoxMenuItem lernToggle = new JCheckBoxMenuItem(
								"Lerned");
						lernToggle.setSelected(patt.isLearned());
						pmen.add(lernToggle);
						lernToggle.addChangeListener(new ChangeListener() {
							public void stateChanged(final ChangeEvent e) {
								patt.setLerned(lernToggle.isSelected());
								repaint();
								updateNet();
							}
						});
						pmen.add("Focus").addActionListener(
								new ActionListener() {
									public void actionPerformed(
											final ActionEvent e) {
										setFocus(patt);
									}
								});
						pmen.add("Edit").addActionListener(
								new ActionListener() {
									public void actionPerformed(
											final ActionEvent e) {
										PatternEditor.show(
												AppPanel.this.getRootPane(),
												patt, editorFinished);
									}
								});
						pmen.add("Copy").addActionListener(
								new ActionListener() {
									public void actionPerformed(
											final ActionEvent e) {
										final Pattern pcop = patt.clone();
										pcop.setLerned(false);
										listModel.addElement(pcop);
										list.setSelectedValue(pcop, true);
									}
								});
						pmen.add("Delete").addActionListener(
								new ActionListener() {
									public void actionPerformed(
											final ActionEvent e) {
										listModel.removeElement(patt);
										AppPanel.this.updateNet();
									}
								});
					}
				}
				list.setSelectedIndex(-1);
				pmen.add("New").addActionListener(new ActionListener() {
					public void actionPerformed(final ActionEvent e) {
						final Pattern p = Pattern.create(patternWidth,
								patternHeight);
						listModel.addElement(p);
						PatternEditor.show(AppPanel.this.getRootPane(), p,
								editorFinished);
						list.setSelectedValue(p, true);
						AppPanel.this.updateNet();
					}
				});
				pmen.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

	private Pattern disturbPattern(final Pattern patt) {
		final Random r = new Random();
		for (int x = 0, mx = patt.getWidth(); x < mx; x++) {
			for (int y = 0, my = patt.getHeight(); y < my; y++)
				if (r.nextDouble() < disturbFactor) {
					patt.setValue(x, y, !patt.getValue(x, y));
				}
		}
		return patt;
	}

	private HVector fromPattern(final Pattern patt) {
		final HVector hv = HVector.create(patt.getWidth() * patt.getHeight());
		for (int x = 0, mx = patt.getWidth(); x < mx; x++) {
			for (int y = 0, my = patt.getHeight(); y < my; y++) {
				hv.setValue(y * mx + x, patt.getValue(x, y));
			}
		}
		return hv;
	}

	private JPanel getLeftPanel() {
		final JPanel p = new JPanel();
		p.setLayout(new BorderLayout());

		pe = new PatternEditor(disturbPattern(getPattern('A')), false);
		p.add(pe, "Center");

		final PatternJListRenderer renderer = new PatternJListRenderer();
		renderer.setShowLerned(false);
		renderer.setIconSize(Math.max(patternWidth, patternHeight) * 4);
		final JList<Pattern> steps = new JList<Pattern>();
		final DefaultListModel<Pattern> stepModell = new DefaultListModel<Pattern>();
		steps.setCellRenderer(renderer);
		steps.setModel(stepModell);
		steps.setVisibleRowCount(1);
		steps.setLayoutOrientation(2);
		p.add(new JScrollPane(steps), "South");

		steps.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(final ListSelectionEvent e) {
				final Pattern sel = steps.getSelectedValue();
				if (sel != null) {
					pe.setPattern(sel);
				}
			}
		});
		pe.setLayout(new BorderLayout());
		final JPanel buttonPanel = new JPanel();
		pe.add(buttonPanel, "South");
		buttonPanel.setOpaque(false);
		buttonPanel.setLayout(new FlowLayout(2));

		final JButton disturb = new JButton("Disturb");
		disturb.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				disturbPattern(pe.getPattern());
				repaint();
			}
		});
		buttonPanel.add(disturb);

		final JButton edit = new JButton("Edit");
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				PatternEditor.show(AppPanel.this.getRootPane(),
						pe.getPattern(), null);
			}
		});
		buttonPanel.add(edit);

		final JButton findPattern = new JButton("Find Pattern");
		findPattern.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				stepModell.clear();

				disturb.setEnabled(false);
				edit.setEnabled(false);
				findPattern.setEnabled(false);

				final HopfieldNet.RecStepCallback callBack = new HopfieldNet.RecStepCallback() {
					public void step(final HVector vec) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								SwingUtilities.invokeLater(new Runnable() {
									public void run() {
										stepModell.addElement(pe.getPattern());
										pe.setPattern(AppPanel.this
												.toPattern(vec));
										AppPanel.this.revalidate();
										AppPanel.this.repaint();
									}
								});
							}
						});
					}
				};
				final Runnable run = new Runnable() {
					public void run() {
						net.findStableFiringPattern(
								AppPanel.this.fromPattern(pe.getPattern()),
								callBack);
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								disturb.setEnabled(true);
								edit.setEnabled(true);
								findPattern.setEnabled(true);
								AppPanel.this.revalidate();
								AppPanel.this.repaint();
							}
						});
					}
				};
				final Thread thread = new Thread(run);
				thread.setDaemon(true);
				thread.setPriority(10);
				thread.setName("Net-Thread");
				thread.start();
			}
		});
		buttonPanel.add(findPattern);

		return p;
	}

	private Pattern getPattern(final char c) {
		final BufferedImage bi = new BufferedImage(patternWidth, patternHeight,
				2);
		final Graphics g = bi.createGraphics();
		g.setColor(Color.WHITE);
		g.setFont(new Font("monospaced", Font.BOLD, (int) (Math.min(
				patternWidth, patternHeight) * 1.5)));

		g.drawString(String.valueOf(c), 0, patternHeight);
		g.dispose();
		final Pattern patt = Pattern.create(patternWidth, patternHeight);
		int x = 0;
		for (final int mx = patt.getWidth(); x < mx; x++) {
			int y = 0;
			for (final int my = patt.getHeight(); y < my; y++) {
				patt.setValue(x, y, bi.getRGB(x, y) != 0);
			}
		}
		return patt;
	}

	private void setFocus(final Pattern patt) {
		pe.setPattern(patt.clone());
	}

	private Pattern toPattern(final HVector vec) {
		final Pattern patt = Pattern.create(patternWidth, patternHeight);
		for (int x = 0, mx = patt.getWidth(); x < mx; x++) {
			for (int y = 0, my = patt.getHeight(); y < my; y++) {
				patt.setValue(
						x,
						y,
						y * mx + x >= vec.getSize() ? false : vec.getValue(y
								* mx + x));
			}
		}
		return patt;
	}

	private void updateNet() {
		final List<HVector> lerned = new ArrayList<HVector>(listModel.getSize());
		for (int i = 0, m = listModel.size(); i < m; i++) {
			final Pattern patt = listModel.getElementAt(i);
			if (patt.isLearned()) {
				lerned.add(fromPattern(patt));
			}
		}
		net = HopfieldNet.createNet(lerned.toArray(new HVector[lerned.size()]));
	}
}
