package cjg.nn.hopfield;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import cjg.nn.hopfield.HopfieldNet.RecStepCallback;

public class Example extends TestCase {

	public static Test suite() {
		final TestSuite suite = new TestSuite();
		suite.addTest(new Example("i4kExample"));
		return suite;
	}

	private Example(final String tst) {
		super(tst);
	}

	public void i4kExample() {
		System.out.println("i4kExample");
		final HVector hv1 = HVector.create(true, false, true, false);
		System.out.println("hv1: " + hv1);
		final HVector hv2 = HVector.create(true, true, false, false);
		System.out.println("hv2: " + hv2);
		final HopfieldNet hn = HopfieldNet.createNet(hv1, hv2);
		System.out.println("Net: \n" + hn);

		final double[][] pair = { { 0, 0, 0, -2 }, { 0, 0, -2, 0 },
				{ 0, -2, 0, 0 }, { -2, 0, 0, 0 } };
		System.out.print("Check:");
		for (int x = 0, m = hn.getSize(); x < m; ++x) {
			for (int y = 0; y < m; ++y) {
				System.out.print(" (" + x + "|" + y + ")");
				assertEquals(pair[x][y], hn.getWeight(x, y));
			}
		}
		System.out.println();

		final HopfieldNet.RecStepCallback between = new RecStepCallback() {
			public void step(final HVector vec) {
				System.out.println("Between: " + vec);
			}
		};
		System.out.println("hv1: ");
		assertEquals(hv1, hn.findStableFiringPattern(hv1, between));
		System.out.println("equals!");
		System.out.println("hv2: ");
		assertEquals(hv2, hn.findStableFiringPattern(hv2, between));
		System.out.println("equals!");
	}
}
